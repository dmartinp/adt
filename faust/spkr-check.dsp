declare name		"speaker_test";
declare version 	"1.0";
declare author 		"AmbisonicDecoderToolkit";
declare license 	"GPL";
declare copyright	"(c) Aaron J. Heller 2016";


speaker_num = hslider("[1]speaker#", 0, 0, 24, 1);

process = _<: par(i, 32, *(i==int(speaker_num))) ;
