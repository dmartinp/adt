declare name		"rerouter";
declare version 	"1.0";
declare author 		"AmbisonicDecoderToolkit";
declare license 	"BSD 3-Clause License";
declare copyright	"(c) Aaron J. Heller 2014";

process(W1, X1, Y1, Z1,
	W2, X2, Y2, Z2,
        W3, X3, Y3, Z3)
  = 
       (W1, W2, W3, 0, 0,
        X1, X2, X3, 0, 0,
        Y1, Y2, Y3, 0, 0,
        Z1, Z2, Z3, 0, 0 );
