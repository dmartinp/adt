function [ outdir ] = ambi_dir( dir_name, create )
    
    if ~exist('create', 'var')
        create = false;
    end
    
    global adt
    
    if isfield(adt, 'install_dir')
        install_dir = adt.install_dir;
    else
        install_dir = '..';
    end
    
    switch dir_name
        case 'decoders'
            outdir = fullfile(install_dir,'decoders');
        case 'data'
            outdir = fullfile(install_dir,'data');
        case 'examples'
            outdir = fullfile(install_dir,'examples');
        case 'python'
            outdir = fullfile(install_dir,'python');
        case 'matlab'
            outdir = fullfile(install_dir,'matlab');
        case 'local'
            outdir = fullfile(install_dir,'local');
    end
    
    if ~exist(outdir,'dir')
        if create
            mkdir(outdir);
        else
            warning('%s directory does not exist: %s', dir_name, outdir);
        end
    end
end
