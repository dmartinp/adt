function [] = write_nfc_filters(max_degree)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    if ~exist('max_degree', 'var') || isempty(max_degree)
        max_degree = 25;
    end
   
    for l = 1:max_degree
        [p,r, err] = BesselPoly(l, true);
        
        %% construct the set of irreducable polynomial factors over the reals
        %   since the coefficients of the polynomial are real (in fact
        %   integers), by the fundamental theorem of algebra, the roots are
        %   either real or appear in complex conjugate pairs. So... the
        %   irreduciable polynomial factors will be first degree for the real
        %   roots and quadratic for complex roots.
        
        % pick out the real and complex roots with positive imaginary parts.
        
        % real roots
        real_root_index = find( imag(r)==0 );
        
        % complex roots with positive imaginary components
        complex_roots_index = find( imag(r)>0 );
        % make sure it is a row vector
        complex_roots_index = complex_roots_index(:).';
        
        % sort by size of imaginary component
        %  (Mathematically, this should not matter, but I think it results
        %   in better numerical performance... might be voodoo.)
        [~, perm_vec] = sort(imag(r(complex_roots_index)), 'descend');
        
        f = {};
        % add the quadratic factors to f
        for i = complex_roots_index(perm_vec)
            f = [f, poly([r(i), conj(r(i))])];
        end
        % add the first-degree factors to f
        for i = real_root_index(:).' % make sure it is a row vector
            f = [f, poly(r(i))];
        end
        
        % plot the roots
        if false
            plot(real(r), imag(r), 'o');
            axis equal
        end
        
        %% print out FAUST for the filters
        fprintf('\n// degree = %d, rel_err = %d\n', l, err(2));
        fprintf('nfcn(%d, radius) = \n', l);
        for i = 1:length(f)
            a = f{i};
            switch length(a)
                case 3
                    fprintf('\t nfc2x(radius, %.15g, %.15g)', a(2), a(3));
                case 2
                    fprintf('\t nfc1x(radius, %.15g)', a(2));
            end
            if i < length(f)
                fprintf(' :\n');
            else
                fprintf(';\n\n');
            end
        end
    end
end

