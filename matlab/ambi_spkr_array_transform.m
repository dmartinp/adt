function [Sout] = ambi_spkr_array_transform(S, varargin)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
   
    nargs = length(varargin);
    
    v = S.r .* [S.x, S.y, S.z];
    
    iarg = 1;
    while iarg <= nargs
        switch lower(varargin{iarg})
            case 'translate'
                xlate = varargin{iarg+1};
                v = v + xlate(ones(1,size(v,1)),:);
                iarg = iarg+2;
            otherwise
                error('unknown transformation %s', varargin{iarg});
        end
    end
    
    Sout = S;
    [Sout.az, Sout.el, Sout.r] = cart2sph(v(:,1), v(:,2), v(:,3));
    [Sout.x, Sout.y, Sout.z] = sph2cart(Sout.az, Sout.el, 1);
    
end

