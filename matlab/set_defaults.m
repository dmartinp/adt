function [ a ] = set_defaults( varargin )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    n_varargin = length(varargin);
    if mod(n_varargin, 2) == 1
        error('unmatched keyword: %s', varargin{end});
    end
    
    for i = 1:2:length(varargin)
        key = varargin{i};
        val = varargin{i+1};
        
        try
            val = evalin('caller', key);
        catch
            assignin('caller', key, val);
        end
        
        if isempty(val)
            assignin('caller', key, val)
        end
            
            
    end

end
