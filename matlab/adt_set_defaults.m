function [] = adt_set_defaults(varargin)
    %UNTITLED3 Summary of this function goes here
    %   Detailed explanation goes here
    
    global adt
    
    switch length(varargin)
        case 0
            % what do we do here?
            %  reset to 'factory defaults'?
        case 1
            s = varargin{1};
            f = fieldnames(s);
            for i = 1:length(f)
                adt.(f{i}) = s.(f{i});
            end
        otherwise
            for i = 1:2:length(varargin)
                adt.(varargin{i}) = varargin{i+1};
            end
    end
end

