function [] = write_atk_yml( filename, D, S, M, C, match_type, decoder_kind, imag_spkrs, alpha, elevation_range)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here

    [Gamma, g0, g0_type] = ambi_shelf_gains(C, S, match_type);

    %% argument defaults
    if isempty(filename)
        fid.empty = 1; % write to console
    else
        [fid.basic,msg] = fopen([filename '-beam-basic-match-amp.yml'],'w');
        [fid.energy,msg] = fopen([filename '-beam-energy-match-' match_type '.yml'],'w');
        [fid.controlled,msg] = fopen([filename '-beam-controlled-match-amp.yml'],'w');
        fid.empty = 0;
        if msg
            error([msg ': ' filename]);
        end
    end

    if false %C.h_order >= 3 || C.v_order >= 3
        warning('The ambiX decoder plugin is limited to 3rd order');
    end

    fprintf('Writing yml config to: %s\n', filename);


    %% header
    fn = fieldnames(fid);
    for k=1:numel(fn)
        fprintf(fid.(fn{k}), 'type : ''decoder''\n\n');
        fprintf(fid.(fn{k}), ['kind : ''ADT' decoder_kind '''\n\n']);
        fprintf(fid.(fn{k}), 'ordering : ''ACN''\n\n');
        fprintf(fid.(fn{k}), 'normalisation : ''N3D''\n\n');
        fprintf(fid.(fn{k}), ['beam : ''' (fn{k}) '''\n\n']);
        if strcmp(decoder_kind,'allrad')
            fprintf(fid.(fn{k}), ['match : ''' match_type '''\n\n']);
        else
            fprintf(fid.(fn{k}), ['match : ''amp''\n\n']);
        end
        if strcmp(decoder_kind,'allrad')
          if isscalar(imag_spkrs)
              fprintf(fid.(fn{k}), ['imagSpeakers : ' num2str(imag_spkrs) '\n\n']);
          else
              fprintf(fid.(fn{k}), 'imagSpeakers : [');
              for i=1:length(imag_spkrs)
                  fprintf(fid.(fn{k}), [num2str(imag_spkrs(i)) ', '])
              end
              fprintf(fid.(fn{k}), ']\n\n');
          end
        end
        if strcmp(decoder_kind,'pinv') || strcmp(decoder_kind,'ssf')
            fprintf(fid.(fn{k}), ['alpha : ' num2str(alpha) '\n\n']);
        end
        if strcmp(decoder_kind,'ssf')
            if isscalar(elevation_range)
                fprintf(fid.(fn{k}), ['elevationRange : ' num2str(elevation_range) '\n\n']);
            else
                fprintf(fid.(fn{k}), 'elevationRange : [');
                for i=1:length(elevation_range)
                    fprintf(fid.(fn{k}), [num2str(elevation_range(i)) ', '])
                end
                fprintf(fid.(fn{k}), ']\n\n');
            end
        end
        fprintf(fid.(fn{k}), ['set : ''HOA' num2str(C.h_order) '''\n\n']);
        fprintf(fid.(fn{k}), 'directions : [\n');
        write_dirs(fid.(fn{k}), S);
        fprintf(fid.(fn{k}), ']\n\n');
        fprintf(fid.(fn{k}), 'matrix : [\n');
    end
    switch D.decoder_type  %#ok<UNRCH>
        case { 2 }
            M = M.lf
    end
    write_rows(fid.energy, ambi_apply_gamma(M, Gamma, C));
    fprintf(fid.energy, ']');
    write_rows(fid.basic, M);
    fprintf(fid.basic, ']');
    write_rows(fid.controlled, ambi_apply_gamma(M, ...
        ambi_in_phase_gains(C, S), C));
    fprintf(fid.controlled, ']');

    %% clean up
    if fid.empty ~=1
        fclose(fid.energy);
        fclose(fid.basic);
        fclose(fid.controlled);
    end
end

function write_rows(thisfid, M)
    for i = 1:size(M,1)
        fprintf(thisfid, '[');
        fprintf(thisfid, '% f, ', M(i,:));
        fprintf(thisfid, '],\n');
    end
end

function write_dirs(thisfid, thisS)
    for i = 1:length(thisS.id)
        fprintf(thisfid, '[');
        fprintf(thisfid, '% f, ', thisS.az(i));
        fprintf(thisfid, '% f', thisS.el(i));
        fprintf(thisfid, '],\n');
    end
end
