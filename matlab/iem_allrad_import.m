function [S, S_imag] = iem_allrad_import(path, spkr_array_name)
    %IEM_ALLRAD_IMPORT - create speaker struct from an JSON file exported from IEM AllRAD plugin
    
    % PATH - path to JSON file
    % NAME - optional name of speaker array, defaults to the filename of
    %        the JSON file (which may not be a "legal" speaker array name).
    
    % reads a JSON file exported by the IEM AllRAD decoder and returns two
    % speaker structs, the first for the real speakers and the second for
    % the imaginary speakers in the array.
    
    % example invocation:
    % >> cd Documents/adt  % (or wherever you keep it)
    % >> adt_initialize;
    % >> [S, S_imag] = iem_allrad_import('examples/Produktionsstudio_o5.json', 'Produktion_Studio');
    
    % NOTE: This ignores the speaker gain parameter in the JSON file.
    
    % Also see:
    %  examples/run_iem_allrad()
    
    
    %% defaults
    if ~exist('spkr_array_name', 'var') || isempty(spkr_array_name)
        [~, spkr_array_name, ~] = fileparts(path);
    end
    
    %% make sure jsonlab is on the path
    if ~exist('loadjson', 'file')
        addpath(fullfile(ambi_dir('matlab'), 'jsonlab-1.5'));
    end
    
    %% 
    %j = jsondecode(fileread(path)); % in recent versions of MATLAB
    j = loadjson(path, 'SimplifyCell', true);
    
    try
        fprintf('importing: %s\ndescription: %s\n', ...
            j.LoudspeakerLayout.Name, ...
            j.LoudspeakerLayout.Description);
        
        L = j.LoudspeakerLayout.Loudspeakers;
    catch ME
        display(ME.msgtext);
    end
    
    % IEM AllRad plugin may scramble speaker order, so sort on Channel 
    [~, sortIdx] = sort([L(:).Channel]);
    L = L(sortIdx);

    A = [[L(:).Azimuth]', [L(:).Elevation]', [L(:).Radius]'];
    
    L_imag = [L(:).IsImaginary];
    
    A_real = A(~L_imag, :);
    A_imag = A(L_imag, :);
    
    ids = arrayfun(@(i)sprintf('S%02i', i), [L(:).Channel],...
            'UniformOutput', false);
    
    S = ambi_mat2spkr_array(A_real, 'AER', 'DDM', ...
        spkr_array_name, ...
        ids(~L_imag));
    S_imag = ambi_mat2spkr_array(A_imag, 'AER', 'DDM', ... 
        [spkr_array_name '_imag'], ...
        ids(L_imag));
   
end

