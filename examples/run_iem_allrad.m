function [D, S, M, C, out_path] = run_iem_allrad(path, spkr_array_name)
    % RUN_IEM_ALLRAD - import IEM JSON speaker array and make an AllRAD decoder
    
    % PATH - path to JSON file
    % SPKR_ARRAY_ NAME - optional name of speaker array, defaults to the 
    %   filename of the JSON file (which may not be a "legal" speaker array 
    %   name).
    
    % sample invocation:
    % >> cd Documents/adt  % (or wherever you keep it)
    % >> adt_initialize;
    % >> run_iem_allrad('examples/Produktionsstudio_o5.json', 'Produktion_Studio');
    
    %% defaults
    if ~exist('spkr_array_name', 'var') || isempty(spkr_array_name)
        [~, spkr_array_name, ~] = fileparts(path);
    end
    
    %% set up channel struct with Ambisonic order and channel order 
    %% and normalization conventions.
    
    C = ambi_channel_definitions(3, 3, 'HV', 'ambix', 'ambix');
    
    %% import the speaker array
    [S, S_imag] = iem_allrad_import(path, spkr_array_name);
    
    % get cartesian coordinates (in meters) of any imaginary speakers
    % x,y,z are unit vectors (I know... not the best names, sorry), so need
    % to multiply them by r
    imag_spkrs = [S_imag.x, S_imag.y, S_imag.z] .* S_imag.r(:, [1,1,1]);
    
    %% run allrad!
    
    [D, S, M, C, out_path] = ambi_run_allrad(S, C, imag_spkrs, [], []);
    
end

