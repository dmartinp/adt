function [ val ] = SPKR_ARRAY_CUBE(radius)
    %ARRAY_CUBE
    %   radius is in meters
    
    if ~exist('radius', 'var') || isempty(radius)
        radius = 1;
    end
    
    val.name = 'cube';
        
    % X Y Z
    S = [
         1  1  1;
         1 -1  1;
        -1 -1  1;
        -1  1  1;
         1  1 -1;
         1 -1 -1;
        -1 -1 -1;
        -1  1 -1;
        ];
    
    S = S * radius/sqrt(3);
    
    % spherical coordinate representation
    [val.az, val.el, val.r] = cart2sph(S(:,1),S(:,2),S(:,3));
    
    % unit vectors
    [val.x, val.y, val.z] = sph2cart(val.az, val.el, 1);
    
    val.id = {'FLU', 'FRU', 'BRU', 'BLU', ...
              'FLD', 'FRD', 'BRD', 'BLD',};
    
end
