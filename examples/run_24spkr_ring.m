function [ D ] = run_24spkr_ring( order )
    
    if ~exist('order', 'var') || isempty(order)
        order = 6;
    end
    
    C = ambi_channel_definitions(order, 0, [], 'acn', 'sn3d');
    S = SPKR_ARRAY_2D_POLYGON(24,2,0,'Horizontal_Ring24');
    
    [D,S,M,C] = ambi_run_pinv(S,C,[],[],[],'HV', 0);
    %[D,S,M,C] = ambi_run_allrad(S, C, [0,0,-1; 0,0,+1], [], true);
    
    D.C = C; D.S = S; 
    
end

