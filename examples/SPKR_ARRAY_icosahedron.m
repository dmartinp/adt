function [ S ] = SPKR_ARRAY_icosahedron()
    %SPKR_ARRAY_icosahedron 20-loudspeaker regular polyhedron
    %   
    
    S = ambdec2spkr_array(...
        '~/.ambdecrc/icosahedron-3h3v.ambdec', ...
        'icosahedron');
end

