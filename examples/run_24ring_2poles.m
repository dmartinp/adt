function [ D ] = run_24ring_2poles( order )
    
    if ~exist('order', 'var') || isempty(order)
        order = 6;
    end
    
    C = ambi_channel_definitions(order, 6, [], 'acn', 'sn3d');
    
    % Horizontal ring of 24 speakers
    Sr = SPKR_ARRAY_2D_POLYGON(24,2,0,'Horizontal_Ring24');
    
    % speakers at the poles
    Sz = ambi_spkr_array('24ring_2poles', 'XYZ', 'MMM', ...
        'T', [0, 0, +2], ...
        'B', [0, 0, -2] ...
        );
    
    S = ambi_spkr_array_join('24ring_2poles', Sr, Sz);
    
    %[D,S,M,C] = ambi_run_pinv(S,C,[],[],[],'HV', 0);
    [D,S,M,C] = ambi_run_allrad(S, C, [0,0,-1; 0,0,+1], [], true);
    
    D.C = C; D.S = S; 
    
end

